module RandomDemo exposing (main)

import Browser
import Html exposing (..)
import Html.Events exposing (..)
import Random



-- MAIN

main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { dieFaces : ( Int, Int )
    }


rndPairCmd : Cmd Msg
rndPairCmd =
    Random.generate NewFaces (Random.pair (Random.int 1 6) (Random.int 7 12))


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model ( 1, 1 )
    , rndPairCmd
    )



-- UPDATE


type Msg
    = Roll
    | NewFaces ( Int, Int )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Roll ->
            ( model
            , rndPairCmd
            )

        NewFaces newFaces ->
            ( Model newFaces
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h2 []
            [ model.dieFaces |> Tuple.first |> String.fromInt |> text
            , text " - "
            , model.dieFaces |> Tuple.second |> String.fromInt |> text
            ]
        , button [ onClick Roll ] [ text "Roll" ]
        ]
