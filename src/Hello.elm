-- define what external code can use from this file


module Hello exposing (main)

-- include Elm modules/libraries

import Array exposing (Array)
import Browser exposing (Document)
import Html exposing (Html)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Random



-- create our initializion for the start of the program
-- a type of datatype/alias Modul


type alias Model =
    Array Int



-- type is like an enum in C
-- define type Msg
-- type Msg has variants (variants have a constructor which are functions)
-- A variant here is for example "Plus Int" or "Minus Int" whereas "Int" describes the arguments of the variant (function)


type Msg
    = Plus Int
    | Minus Int
    | MultiplyBy Int Int
    | Randomize
    | NewNumbers (List Int)


-- INIT
-- initialize type(alias) Model to be an Array (from a List)
-- define type Model


startModelValue : Model
startModelValue =
    Array.fromList [ 10, 20, 30, 40 ]



-- create our VIEW model
-- a view model displays data and datastructures in a formatted way to the user
-- our view model is of type model and gives out a Html Message
-- a Html Message usually includes events like when the user pressed a button or gave input in a textbox
-- if its lowercase it is typevariable (equivalent to class constructor?)
-- we need a type of Array, which gives us the ability to have indexes for each entry in order to know which index to change
-- for that to work we will use Array.indexedMap and input our numberButtonComponent function
-- this function again wants two Int in order to give out a Html Message so we need to convert our Array back into a list, which is of type List Int
-- last but not least we create our Html.div which needs Attributs and the Html messages as arguments. Since we have no Attributs, we just go ahead and create an empty one with "[]"


renderView : Model -> Document Msg
renderView model =
    let
        content =
            model
                |> Array.indexedMap numberButtonComponent
                |> Array.toList
                |> Html.div []

        foo =
            Html.button [ onClick Randomize ] [ Html.text "Randomize" ]
    in
    { title = "test", body = [ content, foo ] }



-- numberButtonComponent : Int -> Int -> Html Msg is like a function prototype and gives the program a hint of what the programmer thinks the function should take as arguments and return.
-- it is not necessary for the program to know these but is more of a help for other Programmers and to make code easy to read


numberButtonComponent : Int -> Int -> Html Msg
numberButtonComponent idx num =
    let
        -- VARIABLE CONTAINER
        numText =
            Html.text (String.fromInt num)

        plusButton =
            Html.button [ style "background-color" "red", onClick (Plus idx) ] [ Html.text "+" ]

        minusButton =
            Html.button [ style "background-color" "blue", onClick (Minus idx) ] [ Html.text "-" ]

        multiplyButton factor =
            Html.button [ style "background-color" "white", onClick (MultiplyBy factor idx) ] [ Html.text ("*" ++ String.fromInt factor) ]
    in
    Html.div [] [ minusButton, numText, plusButton, multiplyButton 2, multiplyButton 4 ]



-- create our UPDATE model
-- the update model takes the input from the user and updates the data according to the base model
-- here our update function takes a Html message and a model as arguments and returns our new model
-- a let ... in block is a local scope to define variables in which then can be used for exactly this block only
-- in our case we need to create variables like updateDelta in order to update our values in the given Array index accoringly
-- to figure out which button was pressed, we give each button an index number (from 0 to 7). This index (var "Idx") number is then passed along with our Html message to this update function
-- to extract the value in our array given by "Idx", we need to Array.get function, which returns a 'boolean', wether the given number exists in the array or not. If it does exists, it will return the value
-- otherwise we tell it to just take 0
-- our add-hoc function at the end of the pipe ("|>") will take the index and set the value of model accordingly


updateModel : Msg -> Model -> ( Model, Cmd Msg )
updateModel msg model =
    let
        updateDelta : Int -> Int -> Model
        updateDelta idx delta =
            model
                |> Array.get idx
                |> Maybe.withDefault 0
                |> (\old -> Array.set idx (old + delta) model)

        updateMultiply : Int -> Int -> Model
        updateMultiply idx factor =
            model
                |> Array.get idx
                |> Maybe.withDefault 0
                |> (\old -> Array.set idx (old * factor) model)
    in
    -- increase index by one using the Plus and Idx arguments
    case msg of
        Plus idx ->
            ( updateDelta idx 1, Cmd.none )

        -- decrease index by one using the Minus and Idx arguments
        Minus idx ->
            ( updateDelta idx -1, Cmd.none )

        MultiplyBy factor idx ->
            ( updateMultiply idx factor, Cmd.none )

        Randomize ->
            ( model, rndNumbersCmd (Array.length model) )

        NewNumbers newNumbers ->
            ( Array.fromList newNumbers
            , Cmd.none
            )


-- separate function to create our random numbers
-- the function takes two arguments as input, Int and a Cmd Msg
rndNumbersCmd : Int -> Cmd Msg
rndNumbersCmd n =
    -- Random.generate NewNumbers (Random.list n (Random.int 1 100))
    Random.generate NewNumbers
    -- we need to put n-2 and n+2 in brackets so it only returns it as one
        (Random.int (n - 2) (n + 2)
            |> Random.andThen
                (\len ->
                    let
                    -- define our "l" variable, which will return the current size of our array indexes
                        numIndexes =
                        -- if len should be randomly generated for less than 1, it should be one, otherwise go for len
                            if len < 1 then
                                1

                            else
                                len
                    in
                    -- randomize for each index in the array from 1 to 100
                    Random.list numIndexes (Random.int 1 100)
                )
        )



-- subscriptions are like interfaces to inputs like the mouse location or hover time over something
-- we dont need these in our example, so we just return them with Sub.none

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none

-- create our Broswer.Document
-- our main program starts here. It uses a pre-defined modul (Browser) main with our very basic three functions "init (base model), view and update"
-- see https://package.elm-lang.org/packages/elm/browser/latest/Browser#application for Browser definitions
main : Program () Model Msg
main =
    Browser.document
        { init = \_ -> ( startModelValue, Cmd.none )
        , view = renderView
        , update = updateModel
        , subscriptions = subscriptions
        }
